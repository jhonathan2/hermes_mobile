function encuesta() 
{
  app.sinBackButton();
  var idBeneficiario = parseInt(GET("id"));
  $("#aAlert").on("click", function(evento)
  {
    evento.preventDefault();
    alert($("#txtEncuesta_Fecha").val());
    alert($("#txtEncuesta_Hora").val());
  })
  $("#txtEncuesta_Prefijo").val(obtenerPrefijo());

  $("#txtEncuesta_Fecha").val(obtenerFecha());

  var Ubicacion = JSON.parse(localStorage.getItem('hd_prog_Ubic'));

    odbc.ejecutarSQL("SELECT * FROM Encuesta WHERE idProgramacion = ? AND codDepartamento = ? AND codMunicipio = ?", [idBeneficiario, Ubicacion.Dep, Ubicacion.Mun], 
            function(rs)
            { 
              var fecha = obtenerFecha();
              var coordenadas = obtenerCoordenadas(function(coordenada)
                {
                  if (rs.length == 0)
                  {
                    var datos = [$("#txtEncuesta_Prefijo").val(), idBeneficiario, Ubicacion.Dep, Ubicacion.Mun, fecha, Usuario.idLogin, coordenada.obj];
                    odbc.ejecutarInsert("INSERT INTO Encuesta (Prefijo, idProgramacion, codDepartamento, codMunicipio, fechaInicio, idLogin, CoordenadasIni) VALUES (?, ?, ?, ?, ?, ?, ?)", datos);
                  } else
                  {
                    var datos = [$("#txtEncuesta_Prefijo").val(), fecha, Usuario.idLogin, coordenada.obj, idBeneficiario];
                    odbc.ejecutarInsert("UPDATE Encuesta SET Prefijo = ?, fechaInicio = ?, idLogin = ?, CoordenadasIni = ? WHERE idProgramacion = ?" , datos);
                  }
                });
            });
    $("#btnEncuesta_Cerrar").attr("idBeneficiario", idBeneficiario);
    $("#btnEncuesta_Cerrar").on("click", btnEncuesta_Cerrar_Click);
    $("#btnFirma_Borrar").on("click", borrarFirma);
    $(".btnIniciarItem").on("click", function()
      {
        $(this).find(".Encuesta_Ok").remove();
      });

    firma_init();

    
    $("#btnEncuesta_Fotos").on("click", abrirCamara);
    $("#btnEncuesta_Archivos").on("click", abrirArchivo);
    $("#btbEncuesta_GuardarFirma").on("click", function(evento)
      {
        evento.preventDefault();
        //guardarFirma();
        $.mobile.changePage("#p7_2");
      });

    $(".encuesta-link").on("click", evaluarPagina);
    
}
function abrirCamara(evento)
{
  evento.preventDefault();
  navigator.camera.getPicture(cameraSuccess, cameraFail, 
    { quality: 80,
    destinationType: Camera.DestinationType.NATIVE_URI,
    saveToPhotoAlbum : true });
}
function abrirArchivo(evento)
{
  evento.preventDefault();
  navigator.camera.getPicture(cameraSuccess, cameraFail, 
    { quality: 80,
      sourceType: 0,
    destinationType: Camera.DestinationType.NATIVE_URI,
    saveToPhotoAlbum : true });
}

function cameraSuccess(imageURI) 
{
    var pPrefijo = obtenerPrefijo();

    var tds = '<div class="imagenTomada">';
              tds += '<img src="" alt="Photo" id="image_' + pPrefijo + '"/>';
    tds += '</div>';

    $(".contenedorImagenesTomadas").append(tds);

    odbc.ejecutarInsert("INSERT INTO Fotos (idFoto, Ruta, Proceso, Prefijo) VALUES (?, ?, ?, ?)", [pPrefijo, imageURI, "Soporte", $("#txtEncuesta_Prefijo").val()]);
    
    var image = document.getElementById('image_' + pPrefijo);

    image.src = imageURI;

    //uploadPhoto(imageURI, pPrefijo, Proceso);
}

function cameraFail(message) 
{
    alert('Failed because: ' + message);
}

function evaluarPagina(evento)
{
  evento.preventDefault();
  objContenedor = $(this).parent("div").parent("div").attr("id");
  var objListado = $("#" + objContenedor).find(".encuesta_item");
  var Evaluacion = true;
  $.each(objListado, function(index, val) 
  {
    
     if ($(val).attr('required') != null)
     {
        if ($(val).attr("encuesta-tipo") == "radio")
        {
          if ($(val).find("input:checked").val() == null)
          {
            Mensaje("Error", "El campo " + $(val).attr("placeholder") + ", debe ser diligenciado");
            $(val).focus();
            Evaluacion = false;
          }
        } else
        {
          if ($(val).val() === null || $(val).val() === "")
          {
            Mensaje("Error", "El campo " + $(val).attr("placeholder") + ", debe ser diligenciado");
            $(val).focus();
            Evaluacion = false;
          }
        }
     }
     return Evaluacion;
  });
  if (Evaluacion)
  {
    if ($(this).attr("ultimo") != null)
    {
      
      var idBtn = $(this).attr("ultimo");
      
      $("#" + idBtn).append('<div class="Encuesta_Ok">Ok</div>');
      var obj = $(".Encuesta_Ok");
      /*
      if (obj.length == 5)
      {
        $("#lblEncuesta_Estado strong").text("Completa");
        $("#lblEncuesta_Estado strong").css("color", "green");
      } else
      {
        $("#lblEncuesta_Estado strong").text("Incompleta");
        $("#lblEncuesta_Estado strong").css("color", "red");
      }
      */
    }   
    $.mobile.changePage($(this).attr("encuesta-direccion"));

  }
}
function btnEncuesta_Cerrar_Click(evento)
{
  evento.preventDefault();
  var objListado = $(".encuesta_item");

  
  var Ubicacion = JSON.parse(localStorage.getItem('hd_prog_Ubic'));
  var codDepMun = Ubicacion.Dep + "" + Ubicacion.Mun;
  var Resultado = "";
  $.each(objListado, function(index, val) 
  {
    var Valor = 1;
    if ($(val).attr("encuesta-tipo") == "radio")
    {
      if ($(val).find("input:checked").val() != null)
      {
        Valor = $(val).find("input:checked").val();
      }
    } else
    {
      Valor = $(val).val();
    }
    if ($(val).attr("encuesta-numeral") != "undefined")
    {
      Resultado += $(val).attr("encuesta-numeral") + "->" + Valor + "##";
    }
  });
  
  var idBeneficiario = parseInt($(this).attr("idBeneficiario"));

  if ($("#txtEncuesta_Tipo").val() == "Visita")
  {
    guardarFirma();
  }

  var fecha = obtenerFecha();

  var coordenadas = obtenerCoordenadas(function(coordenada)
    {
      odbc.ejecutarInsert("UPDATE Encuesta SET fechaFin = ?, Resultado = ?, CoordenadasFin = ? WHERE idProgramacion = ? AND codDepartamento = ? AND codMunicipio = ?" , [fecha, Resultado, coordenada.obj, idBeneficiario, Ubicacion.Dep, Ubicacion.Mun], function()
        {
          odbc.ejecutarInsert("UPDATE Beneficiarios SET Contrato = ? WHERE idBeneficiario = ? AND CodMunicipio = ?" , ['Ejecutado', idBeneficiario, codDepMun], function()
            {
              window.location.replace("home.html");
            });
        });
    });
}