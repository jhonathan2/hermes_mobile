function arranqueDepurar()
{
    //$("#tblTabla").table( "option", "defaults" );
    $("#btnConectar").on("click", function(evento)
    {
        evento.preventDefault();

        var nomBd = "hermes";
        if (nomBd != "")
        {
          var Sentencia = $("#txtSentencia").val();

          //db = openDatabase(nomBd, "1.0", "", 5*1024*1024);

          var Parametros = []

          if (Sentencia == "")
          {
            Sentencia = "select TBL_NAME AS Tabla, SQL from sqlite_master WHERE TBL_NAME <> ?";
            Parametros = ["__WebKitDatabaseInfoTable__"];

          }
            odbc.ejecutarSQL(Sentencia, Parametros, function(datos)
            {
                $("#tblTabla thead tr").remove();
                $("#tblTabla tbody tr").remove();
                if (datos.length > 0)
                {
                    var tbody = "";
                    var titulos = (Object.keys(datos[0]));
                        
                    var thead = "<tr>";
                        $.each(titulos, function(index, val) 
                        {
                            thead += "<th>" + val + "</th>";
                        });
                        thead += "</tr>";
                    
                    var maximo = datos.length;
                      var idx = 0;
                      for (idx = 0; idx < maximo; idx++) 
                      {
                        tbody += "<tr>";
                        $.each(titulos, function(index, val) 
                        {
                            tbody += "<td>" + datos[idx][val] + "</td>";
                        });
                        tbody += "</tr>";
                      };

                    $("#tblTabla thead").append(thead);
                    $("#tblTabla tbody").append(tbody);
                    $("#tblTabla").table("rebuild");
                }
            }, function (tx, error)
{
  alert("Error: "+ error.message)
});
        }
    });
}