$(document).on("ready", index_arranque);

function index_arranque()
{
    app.sinBackButton();
    $("#frmLogin").on("submit", frmLogin_Submit);
}
function frmLogin_Submit(evento)
{
    evento.preventDefault();
    if (validarObligatorios($("#frmLogin")))
    {
        var cDate = new Date();
        
          var pUsuario = $("#txtLogin_Usuario").val().toLowerCase();
          var pClave = $("#txtLogin_Clave").val();
          pClave = md5(md5(md5(pClave)));
          
          odbc.ejecutarSQL('SELECT * FROM Login WHERE Login.Usuario = ? AND Login.Clave = ?', [pUsuario, pClave], 
            function(rs)
            { 
              if (rs.length != 0)
              {
                rs[0].cDate = cDate;
                localStorage.setItem("MinTIC_HD", JSON.stringify(rs[0]));  

                //$.mobile.changePage("home.html");
                window.location.replace("home.html");
              } else
              {
                Mensaje("Error", "Acceso denegado.");
              }
            });
    }
}