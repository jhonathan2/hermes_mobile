var Usuario = JSON.parse(localStorage.getItem('MinTIC_HD'));

var app = {
    cerrarSesion:function()
    {
      delete localStorage.MinTIC_HD;
      window.location.replace("index.html");
    },
    iniciar: function() 
    {
        sincronizacionProgramada();

    },
    instalarSQL: function()
    {   
        crearTabla("versionBD", "fecha");
        crearTabla("Login", "idLogin, Usuario, Clave, Nombre, Imagen");
        crearTabla("Beneficiarios", "idBeneficiario, Nombre, Documento, Direccion, Barrio, Telefono, Celular, Estrato, Departamento, Municipio, Correo, Genero, Contrato, CodMunicipio");
        crearTabla("Programacion", "idProgramacion, idBeneficiario, idLogin");
        crearTabla("Encuesta", "Prefijo, idProgramacion, codDepartamento, codMunicipio, fechaInicio, fechaFin, idLogin, Resultado, CoordenadasIni, CoordenadasFin");
        crearTabla("Fotos", "idFoto, Ruta, Proceso, Prefijo");
        crearTabla("Municipios", "CodDepartamento, CodMunicipio, NomMunicipio");

        odbc.ejecutarSQL("SELECT fecha FROM versionBD", [], 
            function(rs)
            { 
              if (rs.length == 0)
              {
                odbc.ejecutarInsert("INSERT INTO versionBD (fecha) VALUES (?)", ["2015-01-01 00:00:00"]);
              }
            });
    },
    sincronizar:function(ruta, parametros, callback, siError)
    {
        if (parametros === undefined)
        {parametros = {};}

        if (siError === undefined)
            {siError = function(){};}

        if (callback === undefined)
            {callback = function(){};}
        $.post("https://hd.wspcolombia.com/movil/php/" + ruta, parametros, function(data)
        {
            callback(data);
        } , "json"). fail(function()
        {
            siError();
        });
    },
    sinBackButton:function()
    {
      document.addEventListener("backbutton", function(e)
      { 
            e.preventDefault(); 
      }, false);
    }
};
function CompletarConCero(n, length)
{
   n = n.toString();
   while(n.length < length) n = "0" + n;
   return n;
}
function Mensaje(Titulo, Mensaje)
{
  $.gritter.add({
        title: Titulo,
        text: Mensaje
      });
}
function obtenerCoordenadas(callback)
{
  if (callback === undefined)
            {callback = function(){};}

  var objCoordenadas ="";
  navigator.geolocation.getCurrentPosition(
    function(datos)
    {
      var lat = datos.coords.latitude;
      var lon = datos.coords.longitude;
      var accu = datos.coords.accuracy;

      objCoordenadas =  lat + "," + lon + "#" + accu;
      callback({latitud: lat, longitud: lon, obj: objCoordenadas, error:0});
    }, 
    function ()
    {
      objCoordenadas ="No hay precision en el dato";
      callback({latitud: null, longitud: null, obj: null, error:1});
    });
  //return objCoordenadas;
}
function obtenerPrefijo()
{
  var f = new Date();
    return f.getFullYear() + CompletarConCero(f.getMonth() +1, 2) + CompletarConCero(f.getDate(), 2) + CompletarConCero(f.getHours(), 2) + CompletarConCero(f.getMinutes(), 2) + CompletarConCero(f.getSeconds(), 2) + CompletarConCero(Usuario.idLogin, 3);
}
function obtenerFecha()
{
    var f = new Date();
    return f.getFullYear() + "-" + CompletarConCero(f.getMonth() +1, 2) + "-" + CompletarConCero(f.getDate(), 2) + " " + CompletarConCero(f.getHours(), 2) + ":" + CompletarConCero(f.getMinutes(), 2) + ":" + CompletarConCero(f.getSeconds(), 2);
}
function sincronizacionProgramada()
{
  var objResultado = true;
  odbc.ejecutarSQL("SELECT fecha FROM versionBD", [], 
        function(rs)
        { 
          var objFecha = rs[0].fecha;

          app.sincronizar("sincronizarUsuarios.php", {fecha : objFecha}, function(data)
            {
              if (data != 0)
              {
                $.each(data, function(index, val) 
                {
                  if (val.Tipo == "Nuevo")
                  {
                    odbc.ejecutarInsert("INSERT INTO Login (idLogin, Usuario, Clave, Nombre, Imagen) VALUES (?, ?, ?, ?, ?)", [val.idLogin, val.Usuario, val.Clave, val.Nombre, val.Imagen]);
                  } else
                  {
                    odbc.ejecutarInsert("UPDATE Login SET Usuario = ?, Nombre = ?, Clave = ?, Imagen = ? WHERE idLogin = ?", [val.Usuario, val.Nombre, val.Clave, val.Imagen, val.idLogin]);
                  }
                });
              }

              odbc.ejecutarSQL("SELECT * FROM Encuesta", [], 
              function(Encuestas)
              {
                if (Encuestas.length > 0)
                {
                  app.sincronizar("sincronizarEncuestas.php", {datos : Encuestas}, function(data)
                  {
                      if (data != 0)
                      {
                        //odbc.ejecutarInsert("DELETE FROM Encuestas");
                      } 
                  });
                }

                    objFecha = obtenerFecha();
                    odbc.ejecutarInsert("UPDATE versionBD SET fecha = ?", [objFecha]);
                    $("#lblUltimaActualizacion span").text(objFecha);

                    odbc.ejecutarSQL("SELECT * FROM Encuesta", [], 
                    function(Encuestas)
                    {
                        $("#lblEncuestas strong").text(Encuestas.length);
                    });

                  odbc.ejecutarSQL("SELECT * FROM Fotos", [], 
                    function(Fotos)
                    {
                        $("#lblFotos strong").text(Fotos.length);
                    });
              }, function()
              {
                objResultado = false
              });
            }, function()
            {
              objResultado = false;
            });

          bucleBeneficiarios();

          bucleFotos();
        });
} 
function validarObligatorios(objForm)
{
  var rta = true;
  var obj = $(objForm).find(':required');
  $.each(obj, function(index, val) 
  {
     if ($(val).val() == null || $(val).val() == "")
     {
        Mensaje("Error,", "El campo " + $(val).attr("placeholder") + " es obligatorio");
        $(val).focus();
        rta = false;
        return rta;
     }
  });
  return rta;
}
HTTP_GET_VARS=new Array();
strGET=document.location.search.substr(1,document.location.search.length);
if(strGET!='')
    {
    gArr=strGET.split('&');
    for(i=0;i<gArr.length;++i)
        {
        v='';vArr=gArr[i].split('=');
        if(vArr.length>1){v=vArr[1];}
        HTTP_GET_VARS[unescape(vArr[0])]=unescape(v);
        }
    }

function GET(v) {
  if(!HTTP_GET_VARS[v]){return 'undefined';}
  return HTTP_GET_VARS[v];
}

function uploadPhoto(imageURI, Prefijo, idFoto) 
{
  
  var options = new FileUploadOptions();
  options.fileKey="file";
  options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
  options.mimeType="image/jpeg";
  
  var params = {};
  params.value1 = "test";
  params.value2 = "param";
  //alert("Se va a enviar la Foto: " + idFoto);

  if (imageURI.substr(0, 7) == "content")
  {
    window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, function(r)
      {      }, 
      function(error)
      {      //alert("Request: "+ error.code);
      });
     window.resolveLocalFileSystemURI(imageURI, function(fileEntry) 
     {
      imageURI = fileEntry.toURL();
      //alert("La url se convirtió a: " + imageURI);
     }, function()
     {
        //alert("resolve: "+ error.code);
        ejecutarInsert("UPDATE Fotos SET idFoto = ? WHERE idFoto = ?", ["$" + idFoto, idFoto]);
     });      
  }
  
  options.params = params;
  
  var ft = new FileTransfer();

  ft.upload(imageURI, "https://hd.wspcolombia.com/subir/upload.php?Ruta=" + Prefijo, function(r)
    {
      if (r.responseCode == 200)
      {
        if (Prefijo != undefined)
        {
          $.post('https://hd.wspcolombia.com/subir/comprobarArchivo.php', {pPrefijo: Prefijo, pArchivo : imageURI}, 
          function(data, textStatus, xhr) 
          {
              if (data == 1)
              {
                odbc.ejecutarSQL("DELETE FROM Fotos WHERE idFoto = ?", [idFoto]);
                bucleFotos();
              }
          });
        }
      }
    }, function(error)
    {
     if (error.code == 1)
      {
        ejecutarInsert("UPDATE Fotos SET idFoto = ? WHERE idFoto = ?", ["$" + idFoto, idFoto]);
        bucleFotos();
      } 
    }, options);
  
}
function sincronizarBeneficiarios(objFecha, objCantidad)
{
  app.sincronizar("sincronizarBeneficiarios.php", {fecha : objFecha, cantidad : objCantidad}, function(data)
  {
    if (data.Cantidad != undefined)
    {
     
      $.each(data.Datos, function(index, val) 
      {
        //alert(val.idBeneficiario);
          odbc.ejecutarInsert("INSERT INTO Beneficiarios (idBeneficiario, Nombre, Documento, Direccion, Barrio, Telefono, Celular, Estrato, Departamento, Municipio, Correo, Genero, Contrato, CodMunicipio) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [parseInt(val.idBeneficiario), val.Nombre, val.Documento, val.Direccion, val.Barrio, val.Telefono, val.Celular, val.Estrato, val.Departamento, val.Municipio, val.Correo, val.Genero, val.Contrato, val.CodMunicipio],
            function(){}, function (tx, error)
            {
              alert(error.message)
            });
      });
      if (objCantidad < data.Cantidad)
      {
        bucleBeneficiarios();
      } else
      {
        
      }
    } else
    {
      //alert("Error: " + data);
    }
  }, function()
  {
    // Si error
  });
}

function bucleBeneficiarios()
{
  odbc.ejecutarSQL("SELECT fecha FROM versionBD", [], 
    function(rs)
    { 
      var objFecha = rs[0].fecha;
      odbc.ejecutarSQL("SELECT COUNT(*) AS Cantidad FROM Beneficiarios", [], 
      function(rs)
      { 
        var objCantidad = rs[0].Cantidad;
        sincronizarBeneficiarios(objFecha, objCantidad);
      });
    });
}
function bucleFotos()
{
  ejecutarSQL('SELECT * FROM Fotos WHERE idFoto NOT LIKE ?', ["$%"],
    function(rs)
    {
      $("#lblFotos strong").text(rs.length);
    });

  ejecutarSQL('SELECT * FROM Fotos WHERE idFoto NOT LIKE ? limit 0, 1', ["$%"],
  function(fotos)
  {
    $.each(fotos, function(index, value)
      {
        uploadPhoto(value.Ruta, value.Proceso + "/" + value.Prefijo, value.idFoto);
      });
  });
}