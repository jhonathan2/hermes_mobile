var db = openDatabase("hermes", "1.0", "Base de datos de Hermes", 5*1024*1024);
var odbc = 
  {
    InsertarMasivo:function (sentencia, datos, callback)
    {
        if (callback === undefined)
            {callback = function(){};}
      $.each(datos, function(index, val) 
      {
         odbc.ejecutarInsert(sentencia, val);
      });
      callback();
    },
    ejecutarInsert:function (sentencia, datos, callback, siError)
    {
      if (siError === undefined)
            {siError = errorCB;}

      if (callback === undefined)
            {callback = function(){};}

      db.transaction(function(tx) 
      {
        tx.executeSql(sentencia, datos, callback, siError);
      });
    },
    ejecutarSQL:function (sentencia, parametro, callback, siError)
    { 
      if (siError === undefined)
            {siError = errorCB;}

      if (callback === undefined)
            {callback = function(){};}

      db.transaction(function(tx) 
      {
        tx.executeSql(sentencia, parametro, 
          function(tx, rs)
          {
            var data = [];
            var maximo = rs.rows.length;
                  var idx = 0;
                  for (idx = 0; idx < maximo; idx++) 
                  {
                    data.push(rs.rows.item(idx));
                  };
            callback(data);
          }
          , siError);
      });
    },
  };


function crearTabla(Tabla, Campos)
{
  var sentencia = 'CREATE TABLE IF NOT EXISTS ' + Tabla + ' (' + Campos + ')';
  odbc.ejecutarSQL(sentencia);
}


function errorCB(tx, error)
{
  console.log(error.message)
}
