function arranqueBeneficiario()
{
  app.sinBackButton();

  var idBeneficiario = parseInt(GET('id'));
  var codDepto = GET('Mun');

  $("#txtBeneficiario_Id span").text(idBeneficiario);

  $(".btnBeneficiario_Modificar").on("click", modificarValor);

  odbc.ejecutarSQL("SELECT * FROM Beneficiarios INNER JOIN Municipios ON Beneficiarios.Departamento = Municipios.CodDepartamento AND Beneficiarios.Municipio = Municipios.CodMunicipio WHERE Beneficiarios.idBeneficiario = ? AND Beneficiarios.CodMunicipio = ?", [idBeneficiario, codDepto], 
            function(rs)
            { 
              var Beneficiario = rs[0];
              $("#txtBeneficiario_Id span").text(Beneficiario.idBeneficiario);
              $("#txtBeneficiario_Nombre span").text(Beneficiario.Nombre);
              $("#txtBeneficiario_Cedula span").text(Beneficiario.Documento);
              $("#txtBeneficiario_Direccion span").text(Beneficiario.Direccion);
              $("#txtBeneficiario_Barrio span").text(Beneficiario.Barrio);
              $("#txtBeneficiario_Telefono span").text(Beneficiario.Telefono);
              $("#txtBeneficiario_Celular span").text(Beneficiario.Celular);
              $("#txtBeneficiario_Estrato span").text(Beneficiario.Estrato);
              $("#txtBeneficiario_Departamento span").text(Beneficiario.Departamento);
              $("#txtBeneficiario_Municipio span").text(Beneficiario.NomMunicipio);
              $("#txtBeneficiario_Correo span").text(Beneficiario.Correo);
              $("#txtBeneficiario_Contrato span").text(Beneficiario.Contrato);

              if (Beneficiario.Contrato == 518)
              {
                $("#txtBeneficiario_Contratista span").text("TELMEX DE COLOMBIA S.A");
              }

              if (Beneficiario.Contrato == 519)
              {
                $("#txtBeneficiario_Contratista span").text("EMPRESA TELECOMUNICACIONES DE BUCARAMANGA S.A ESP");
              }

              if (Beneficiario.Contrato == 520)
              {
                $("#txtBeneficiario_Contratista span").text("COLOMBIA TELECOMUNICACIONES S.A ESP");
              }
              
              $("#txtBeneficiario_Velocidad span").text(1);

              $("#btnIniciarioEncuesta").attr("href", "encuesta.html?id=" + idBeneficiario);
              
            });
}

function modificarValor()
{
    var obj = $(this);
    var contenedor = $(obj).parent("div");
    var objSpan = $(contenedor).find('span');
      //location.reload();
    var person = prompt("Por favor ingrese los valores para reemplazar el campo.", $(objSpan).text());
    if (person != null) 
    {
        $(objSpan).text(person);
    }
} 